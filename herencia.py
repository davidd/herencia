class Empleado:
    
    def __init__(self, nom, salario):
        self.nombre = nom
        self.nomina = salario

    def calcula_impuestos (self):
        return self.nomina*0.30

    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre,
                tax=self.calcula_impuestos())


class Jefe(Empleado):
    def __init__(self, nom, salario, prim):
        super().__init__(nom, salario)
        self.prima = prim

    def calculo_prima(self):
        prima_total = self.nomina * (self.prima / 100)
        return prima_total
    
    def calculo_impuestos1(self):
        impuestos_final = (self.calculo_prima() + self.nomina) * 0.30
        return impuestos_final
    
    def __str__(self):
        return "El empleado {name} con nomina {nomin:.2f} y prima del {pri} por ciento debe pagar {tax:.2f}".format(name=self.nombre, nomin=self.nomina, pri=self.prima,
                tax=self.calculo_impuestos1())


David = Jefe("David", 100000, 10)
print(David)